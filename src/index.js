import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import './index.css';
import AppComponent from './App';
import {ProductProvider} from './components/context/context';
import {BrowserRouter as Router} from 'react-router-dom';

ReactDOM.render(
    <ProductProvider>
        <Router>
            <AppComponent />
        </Router>
    </ProductProvider>
, document.getElementById('root'));


serviceWorker.unregister();
