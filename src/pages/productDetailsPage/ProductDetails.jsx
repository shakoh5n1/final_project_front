import React, { Component } from 'react'
import {ProductConsumer} from '../../components/context/context';
import {Link} from 'react-router-dom';
// import {ButtonContainer} from '../../components/Button';
import HeaderComponent from '../../components/header/Header'
import WrapperComponent from '../../components/shared/wrapper/Wrapper'
import ProductService from '../../Service';
import FooterComponent from '../../components/footer/Footer';
import  './ProductDetails.css'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Button from 'react-bootstrap/Button';


export default class Details extends Component {

    constructor(props) {
        super(props);
        this.productService = new ProductService();

        this.state = {
            product:[],
            id : this.props.match.params.id
        }
    }

    componentDidMount() {
        this.setproduct()  
}
    setproduct = () => {
        const id = this.state.id
        this.productService.getDetailProduct(id)
            .then((response) => {
                this.setState(() => {
                return {product:response.Product}
             })
        })
 
    }
    
    render() {
        return (
            <React.Fragment>
                <HeaderComponent />
                <WrapperComponent>
                    
                <ProductConsumer>
                {value =>{
                    const{
                        _id,
                        company,
                        img,
                        info,
                        price,
                        title,
                        inCart,
                        // type
                    } = this.state.product
                    // console.log(this.state.product)
                    // console.log(this.props.match.params.id )
                    return(
                        <div className="container py-5 prdDetails">
                            
                            {/* title */}
                            <div className="row prdTitle">
                                <div className="col-10 mx-auto
                                text-center text-slanted text-blue">
                                    <h1>{title}</h1>
                                </div>
                            </div>

                            {/* end title */}
                            {/* product info */}
                            <div className="row">
                                <div className="col-10 mx-auto col-md-6 my-3">
                                <img src={`http://localhost:5000/${img}`} className="img-fluid imgPrd" alt="product"/>
                                    </div> 
                                    {/* product text */}
                                    <div className="col-10 mx-auto col-md-6 my-3 text-capitalize">
                                        <h2>model: {title}</h2>
                                        <h4 className=" text-uppercase text-muted mt-3 mb-2">
                                        made by : <span className="text-uppercase">
                                            {company}
                                        </span>
                                        </h4>
                                        <h4 className="text-blue">
                                        <strong>
                                            price : <span>$</span>
                                            {price}
                                         </strong>
                                        </h4>
                                        <p className="text-capitalize font-weight-bold mt-3 mb-0">
                                            about product:
                                        </p>
                                        <p className="text-muted lead">
                                            {info}
                                        </p>
                                    
                                        <div>
                                            <Link to='/' style={{ textDecoration: 'none' }}>
                                            <ButtonToolbar>
                                                <Button variant="info">Back to Products</Button>
                                            </ButtonToolbar>
                                    </Link>
                                    <ButtonToolbar>
                                    <Button 
                                    cart
                                    disabled={inCart?true:false}
                                    onClick={()=>{
                                        value.addToCart(_id);
                                        value.openModal(_id);

                                    }}
                                    >
                                        {inCart?"inCart": "add to cart"}
                                    </Button >
                                    </ButtonToolbar>
                                </div>
                                
                                </div> 
                                
                            </div>
                            
                        </div>
                    )
                }}
                </ProductConsumer>
                </WrapperComponent>
                <FooterComponent />
            </React.Fragment>
        );
    }
}