import React from 'react';
import ProductList from '../../components/products/ProductList';
// import Title from '../../components/products/Title';
import HeaderComponent from '../../components/header/Header';
import JumbotronComponent from '../../components/jumbotron/Jumbotron';
import FooterComponent from '../../components/footer/Footer';

export default class ProductListPage extends React.Component {
    render() {
        return(
            <React.Fragment>
                <HeaderComponent />
                <JumbotronComponent />
                {/* <Title name='All' title="Products"/> */}
                <ProductList />
                <FooterComponent />
            </React.Fragment>
        )
    }
}