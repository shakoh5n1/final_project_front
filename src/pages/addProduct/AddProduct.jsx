import React from 'react';
import UserService from '../../Service';
import '../../components/authorization/Authorization.css'
import HeaderComponent from '../../components/header/Header';
import FooterComponent from '../../components/footer/Footer'
 
export default class AddProductComponent extends React.Component {

    constructor() {
        super();

       this.state = {
           
           img: []
       }
        this.userService = new UserService();

        this.addNewProduct = this.addNewProduct.bind(this);
        
    }

    addNewProduct(event) {
       
        event.preventDefault();
       
        

        // let tempProductInfo = {
        //     img : this.state.img,
        //     title : event.target.title.value,
        //     // img : event.target.img.value.replace("C:\\fakepath\\", ""),
        //     price : event.target.price.value,
        //     company : event.target.company.value,
        //     info : event.target.info.value,
        //     inCart: event.target.inCart.value,
        //     count : event.target.count.value,
        //     type: event.target.type.value,
        //     total : event.target.total.value
        // }
        console.log(event.target);
        var formData = new FormData();
        formData.append('img' , event.target.img.files[0]);
        formData.append('title' , event.target.title.value);
        formData.append('price' , event.target.price.value);
        formData.append('company' , event.target.company.value);
        formData.append('info' , event.target.info.value);
        formData.append('inCart' , event.target.inCart.value);
        formData.append('count' , event.target.count.value);
        formData.append('type' , event.target.type.value);
        formData.append('total' , event.target.total.value);
        
        this.userService.addNewProduct(formData)
            .then( (res) => {
                // this.props.detectChange()
                return res
            })
    }


    imgHandler = event => {
        console.log(event.target.files[0])
        this.setState({
            img: event.target.files[0],
           
        })
    }
   
    render() {
        return (
            <React.Fragment>
                <HeaderComponent />
                <div className="logcont locontadd" >
                    <div className="container">
                        <h2 className="mt-5 mb-5">Add New Product</h2>
                        <hr />
                        <br />
                        <form encType='multipart/form-data'   className="form" onSubmit={this.addNewProduct}>
                    {/* <legend>Add New Product</legend> */}
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="text" name="title" placeholder='Product Title' className='form-control'/>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" accept="image/*" name="img" onChange={this.imgHandler} class="custom-file-input" id="inputGroupFile01"aria-describedby="inputGroupFileAddon01" />
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                            </div>
                            {/* <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="file" accept="image/*" name="img" onChange={this.imgHandler} className='form-control'/>
                            </div> */}
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="text" name="price" placeholder= 'price' className='form-control'/>
                            </div>
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="text" name="company" placeholder= 'company' className='form-control'/>
                            </div>
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="text" name="info" placeholder= 'info' className='form-control'/>
                            </div>
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="text" name="inCart" placeholder= 'inCart' className='form-control'/>
                            </div>
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="text" name="count" placeholder= 'count' className='form-control'/>
                            </div>
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="text" name="type" placeholder= 'type' className='form-control'/>
                            </div>
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                                <input type="text" name="total" placeholder= 'total' className='form-control'/>
                            </div>
                            <div className="form-group">
                                <div className="label">
                                    <label className="text-muted"></label>
                                </div>
                            </div>
                            <div className="label-input button-signup">
                                <input type="submit" value="upload"  className="btn btn-raised btn-primary" />
                            </div>
                            
                            
                </form>
                
            </div>
            </div>
            <FooterComponent />
            </React.Fragment>        
        )
    }

}