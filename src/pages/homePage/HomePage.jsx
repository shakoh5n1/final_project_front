import React from 'react';
import HeaderComponent from '../../components/header/Header';
import JumbotronComponent from '../../components/jumbotron/Jumbotron';
import MainComponent from '../../components/main/Main';
// import FilterComponent from '../../components/filter/Filter'
import FooterComponent from '../../components/footer/Footer';

export default class HomePage extends React.Component {
    render() {
        return (
            <React.Fragment>
                <HeaderComponent />
                <JumbotronComponent />
                {/* <FilterComponent /> */}
                <MainComponent />
                <FooterComponent/>
            </React.Fragment>
        )
    }
}
