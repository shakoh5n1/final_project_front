export default class ProductService {

    getProductList() {
        return fetch('http://localhost:5000/zoomer/productlist')
            .then((response) => {
                return response = response.json();
            })
    }

    getDetailProduct(productId) {
        return fetch(`http://localhost:5000/zoomer/details/${productId}`)
            .then((response) => {
                return response = response.json();
            })
    }

    getProductByType() {
        return fetch('http://localhost:5000/zoomer/filter')
            .then((response) => {
                
                return response = response.json();
            })
    }


    addNewProduct(productInformation) {
        console.log(productInformation)
        return fetch('http://localhost:5000/zoomer/addproduct', {
            headers: {
                Accept: 'application/json'
            },
            method: 'POST',
            
            body: productInformation
            // body: productInformation
        }).then((response) => {
            return response = response.json();
        })
    }
    

}




