import React from 'react';
import './Navigation.css';
import UserImg from '../../../src/img/menu_resp_icon.svg';
import {Link} from 'react-router-dom';

export default class NavigationHeaderComponent extends React.Component {
    render() {
        return(
            <React.Fragment>
                <div className="navigationHeader">
                    <img src={UserImg} alt="menu" className="navigationimg fontStyle"/>
                    <span className="navigationMenu fontStyle">Navigation</span>
                    <Link to={'/ptoductlist'} style={{ textDecoration: 'none' }}><span className="navigationlink fontStyle" >All</span></Link>
                </div>
            </React.Fragment>
        )
    }
}
