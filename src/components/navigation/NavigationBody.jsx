import React from 'react';
import './Navigation.css';
import mobIcon from '../../img/mobilephone.svg';

export default class NavigationBodyComponent extends React.Component {
    render() {
        return(
            <React.Fragment>
                <div className="navigationBody">
                    <ul className="navigationList">
                        <li>
                            <img src={mobIcon} alt="" className="listIcons" />
                            <span>Mobile Phone</span>
                        </li>
                        <li>
                            <img src={mobIcon} alt="" className="listIcons" />
                            <span>Mob. Accessories</span>
                        </li>
                        <li>
                            <img src={mobIcon} alt="" className="listIcons" />
                            <span>Tablets</span>
                        </li>
                        <li>
                            <img src={mobIcon} alt="" className="listIcons" />
                            <span>Digital & Video Cameras</span>
                        </li>
                        <li>
                            <img src={mobIcon} alt="" className="listIcons" />
                            <span>Laptops</span>
                        </li>
                        <li>
                            <img src={mobIcon} alt="" className="listIcons" />
                            <span>Gaming Consoles</span>
                        </li>
                        <li>
                            <img src={mobIcon} alt="" className="listIcons" />
                            <span>Audio Systems</span>
                        </li>
                        <li>
                            <img src={mobIcon} alt="" className="listIcons" />
                            <span>Other Products</span>
                        </li>
                    </ul>
                </div>   
            </React.Fragment>
        )
    }
}