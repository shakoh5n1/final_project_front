import React from 'react';
import './Navigation.css';
import NavigationHeaderComponent from './NavigationHeader';
// import NavigationBodyComponent from './NavigationBody';


export default class NavigationComponent extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div className="navigation">
                    <NavigationHeaderComponent />
                    {/* <NavigationBodyComponent /> */}
                </div>
                
            </React.Fragment>
        )
    }
}