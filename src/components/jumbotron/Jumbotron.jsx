import React from 'react';
import './Jumbotron.css';
// import WrapperComponent from '../shared/wrapper/Wrapper'; 
// import NavigationComponent from '../navigation/Navigation';
import SliderComponent from '../slider/Slider';

export default class JumbotronComponent extends React.Component {
    render() {
        return(
            <div className="jumbotronContainer">
                <SliderComponent />
            </div>
        )
    }
}