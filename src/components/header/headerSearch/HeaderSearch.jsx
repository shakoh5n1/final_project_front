import React from 'react';
import './HeaderSearch.css';
import BtnImg from '../../../img/noun_Search.svg';

export default class HeaderSearchComponent extends React.Component {
    render() {
        return(
            <React.Fragment>
                <div className="headerSearch">
                    <form className="headerSearchForm">
                        <input type="text" id="headerSearchInput" name="headerSearchInput" placeholder="Search store" />
                        {/* <div className="searchDropdown">
                            <select className="searchDropdownSelect">
                                <option value="0">Category</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div> */}
                        <button className="HeaderSearchBtn">
                            <img alt="Logo" src={BtnImg} />
                        </button>
                    </form>
                </div>
            </React.Fragment>

        )
    }
}