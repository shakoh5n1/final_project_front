import React from 'react';
import {Link} from 'react-router-dom';
import './HeaderBasket.css';
import BaskImg from '../../../img/noun_Shopping.svg';

export default class HeaderBasketComponent extends React.Component {
    
    render() {
        return (
            <Link to="/cart" className="ml-auto" style={{ textDecoration: 'none' }}>
                <div className="headerBasket">
                    <img src={BaskImg} alt="userprofile" />
                    <div>
                        <span className="basketTitle" >Shopping cart</span>
                        {/* <span className="basketPrice">0 L</span> */}
                    </div>
                </div>
            </Link>
        )
    }
}