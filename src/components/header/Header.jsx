import React from 'react';
import './Header.css';
import WrapperComponent from '../shared/wrapper/Wrapper';
import HeaderLogo from './headerLogo/HeaderLogo';
import HeaderSearchComponent from '../header/headerSearch/HeaderSearch';
import HeaderProfileComponent from '../header/headerProfile/HeaderProfile';

export default class HeaderComponent extends React.Component {
    render() {
        return (
            <React.Fragment>
                {/* <div className="headerTop"></div> */}
                <header className="header">
                    <WrapperComponent>
                        <div className="headerContent">
                            <HeaderLogo />
                            <HeaderSearchComponent />
                            <HeaderProfileComponent />
                        </div>
                    </WrapperComponent>
                </header>
            </React.Fragment>
            
        )
    }
}

