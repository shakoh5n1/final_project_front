import React from 'react';
import './UserProfile.css';
import UserImg from '../../../img/noun_User.svg';
// import RegistrationForm from '../../authorization/Register'
// import Menu from '../../authorization/Menu'



export default class UserProfileComponent extends React.Component {



    render() {
        return (
            <div className='userProfile' onClick={this.props.value}>
                {/* <RegistrationForm/> */}
                <img src={UserImg} alt="userprofile"/>
                {/* <Menu /> */}
                <span>Profile Info</span>
            </div>
        )
    }
}