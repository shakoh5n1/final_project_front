import React from 'react'
import {Link} from 'react-router-dom';
import ListGroup from 'react-bootstrap/ListGroup'

export default function CartTotals({value}) {
    const{cartSubTotal,cartTax,cartTotal} = value;
    return (
        <React.Fragment>
            <div className='container-fluid'>
              <div className="row">
                  <div className="col-10 mt-2 ml-sm-5 ml-md-auto col-sm-8 text-capitalize text-rigth">
                      <Link to="/">
                          {/* <button className="btn btn-outline-danger text-uppercase mb-3 px-5" type="button"
                          onClick={()=>clearCart()}>
                              clear cart
                          </button> */}
                          </Link>

                        <ListGroup variant="flush">
                            <ListGroup.Item>Subtotal $ {cartSubTotal}</ListGroup.Item>
                            <ListGroup.Item>Tax $ {cartTax}</ListGroup.Item>
                            <ListGroup.Item>Total $ {cartTotal}</ListGroup.Item>
                            
                        </ListGroup>
                          {/* <h5>
                              <span className="text-title">
                               subtotal :</span>
                               <strong>$ {cartSubTotal}</strong>
                          </h5> */}
                          {/* <h5>
                              <span className="text-title">
                               Tax :</span>
                               <strong>$ {cartTax}</strong>
                          </h5> */}
                          {/* <h5>
                              <span className="text-title">
                               toral :</span>
                               <strong>$ {cartTotal}</strong>
                               
                          </h5> */}
                  </div>
              </div>
            </div>
        </React.Fragment>
    )
}
