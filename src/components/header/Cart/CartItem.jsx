import React from 'react'
import './Cart.css'

export default function CartItem({item,value}) {
    const{id,title,img,price,total,count} = item;
    const {increment,decrement} = value;
    return (
        <div className="row my-2 text-capitalize text-center cartList">
            <div className="col-10 mx-auto col-lg-2">
                <img 
                src={`http://localhost:5000/${img}`}
                style={{width:'5rem', height:'5rem'}}
                className="img-fluid"
                alt="product"
                />
            </div>
            <div className="col-10 mx-auto col-lg-2 cardPrd">
                <span className="d-lg-none">product : </span>{title}
            </div> 
            <div className="col-10 mx-auto col-lg-2 cardPrd">
                <span className="d-lg-none">price : </span>{price}
            </div> 
            <div className="col-10 mx-auto col-lg-2 my-2 my-lg-0 cardPrd">
                <div className="d-flex justify-content-center cardPrd">
                    <div className="cardPrd">
                        <span className="btn btn-black mx-1 cardPrd" onClick={()=>decrement(id)}>-</span>
                        <span className="btn btn-black mx-1 cardPrd" >{count}</span>
                        <span className="btn btn-black mx-1 cardPrd" onClick={()=>increment(id)}>+</span>
                    </div>
                </div>
            </div>
            {/*  */}
            {/* <div className="col-10 mx-auto col-lg-2">               
                <div className="cart-icon"  onClick={()=>removeItem(id)} >
                    <i className="fas fa-trash"></i> 
                    </div>
                </div> */}
            <div className="col-10 mx-auto col-lg-2">               
                <strong>item total : $  {total}</strong>
               
            </div> 
            </div>            
            

    );
}

