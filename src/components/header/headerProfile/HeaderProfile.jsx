import React from 'react';
import './HeaderProfile.css';
import UserProfileComponent from '../userProfile/UserProfile';
import HeaderBasketComponent from '../headerBasket/HeaderBasket';
import LoginComponent from '../../authorization/Signin';



export default class HeaderProfileComponent extends React.Component {

    constructor(props){
        super(props);

        this.state= {
            display: true   
        }
    }

    onChange() {
        this.setState({
            display: !this.state.display
        })
    }

    onClick = () => {
        this.onChange(); 
    }
    

    render() {
        return (
            
            <div className="headerProfile">
                <div className="userwraper" >
                    <UserProfileComponent value={this.onClick} />
                    <LoginComponent value={this.state.display} />
                </div>
                <HeaderBasketComponent />
            </div>
            
        )
    }
}

