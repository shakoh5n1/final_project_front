import React from 'react';
import {Link} from 'react-router-dom';
import Logo from '../../../img/logoen.png'
import './HeaderLogo.css'

export default class HeaderLogo extends React.Component {
    render() {
        return(
            <Link to="/">
            <h1 className="headerLogo">
                <img alt="Logo" src={Logo} />
            </h1>
            </Link>
            
        )
    }
}