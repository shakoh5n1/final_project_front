import React, { Component } from 'react'
// import { detailProduct} from '../../data';
import ProductService from '../../Service';
const ProductContext = React.createContext();

//Provider
//Consumer
class ProductProvider extends Component {
    constructor(props) {
        super(props);
        this.productService = new ProductService();

        this.state = {
            products: [],
            detailProduct:[],
            type:[],
            cart:[],
            modalOpen:false,
            modalProduct:[],
            cartSubTotal:0,
            cartTax:0,
            cartTotal:0
            
        };
    }
    
    componentDidMount(){
        this.setProdutc();
        // this.filterByType();
        
        
    }
    componentWillUpdate () {
        // this.filterByType()
    }
    componentDidUpdate () {
        // this.filterByType()
        // this.setProdutc()
    }
    
    setProdutc = () =>{

        this.productService.getProductList()
        .then((response) => {
            let tempProducts = []
            response.forEach(item => {
                const singleItem = {...item}
                // console.log(singleItem)
                tempProducts = [...tempProducts,singleItem];
            })
            this.setState(()=>{
                // console.log(tempProducts)
                return {products:tempProducts}
            })
        })
    };

    
    filterByType = () => {
        this.productService.getProductByType()
        .then((response) => {
            console.log(response)
            let filtProducts = []
            response.forEach(item => {
                const singleFiltItem = {...item}
                console.log(singleFiltItem)
                filtProducts = [...filtProducts,singleFiltItem];
            })
            this.setState(()=>{
                console.log(filtProducts)
                return {products:filtProducts}
            })
        })
        
    }
    
    // getItemBytype = (type) => {
    //     const productType = this.state.products.find(item => item.type === type);
    //     console.log(productType)
    // }
    getItem = (id) =>{
        const product = this.state.products.find(item => item._id === id);
        
        return product;
    };

    handleDetail = (id) =>{
        const product = this.getItem(id);
        // console.log(product)
        this.setState(()=>{
            // console.log({detailProduct:product})
            return {detailProduct:product}
        })
        
        // this.productService.getDetailProduct(id)
        // .then((response) => {
        //     // console.log(response)
        // })
    };
   
    addToCart = (id) =>{
        let tempProducts = [...this.state.products];
        const index = tempProducts.indexOf(this.getItem(id));
        const product = tempProducts[index];
        product.inCart = true;
        product.count = 1;
        const price = product.price;
        product.total = price;
        this.setState(
            ()=>{
            return {products:tempProducts,cart:[...this.state.cart,
                product]};      
        },
        ()=>{
        this.addTotals();
        }
        );
    };
    openModal = id =>{
        const product =this.getItem(id);
        this.setState(()=>{
            return {modalProduct:product,modalOpen:true}
        })
    }
    closeModal = () =>{
     this.setState(()=>{
         return {modalOpen:false}
     });
    };
    increment = (id) =>{
       let tempCart = [...this.state.cart];
       const selectedProduct = tempCart.find(item => item.id === id)

       const index = tempCart.indexOf(selectedProduct);
       const product = tempCart[index];

       product.count = product.count +1;
       product.total = product.count * product.price;

       this.setState(
         ()=>{
             return {cart:[...tempCart]};
         },
         () =>{
             this.addTotals();
         }
       );
    }
    decrement = (id) =>{
        let tempCart = [...this.state.cart];
        const selectedProduct = tempCart.find(item => item.id === id)
 
        const index = tempCart.indexOf(selectedProduct);
        const product = tempCart[index];
        product.count = product.count -1;

        if(product.count === 0){
            this.removeItem(id)
        }
        else{
            product.total = product.count * product.price;
            this.setState(
                ()=>{
                    return {cart:[...tempCart]};
                },
                () =>{
                    this.addTotals();
                }
              );
        }
        
    };
    removeItem = (id) =>{
        let tempProducts = [...this.state.products];
        let tempCart = [...this.state.cart];
        tempCart = tempCart.filter(item => item.id !== id);
        const index = tempProducts.indexOf(this.getItem(id));
        let removedProduct = tempProducts[index];
        removedProduct.inCart = false;
        removedProduct.count = 0;
        removedProduct.total = 0;
        this.setState(()=>{
            return{
                cart:[...tempCart],
                product:[...tempProducts]

            }
        },()=>{
            this.addTotals();
        })
    }
    clearCart = ()=>{
        this.setState({
cart:[]
        });

    };
    addTotals = () =>{
        let subTotal = 0;
        this.state.cart.map(item =>(subTotal += item.total));
        const tempTax = subTotal * 0.1;
        const tax = parseFloat(tempTax.toFixed(2));
        const total = subTotal + tax
        this.setState(()=>{
            return{
                cartSubTotal:subTotal,
                cartTax:tax,
                cartTotal:total
            }
        })
    }
    
    render() {
        return (
            
            <ProductContext.Provider value={{
              ...this.state,
              handleDetail:this.handleDetail,
              addToCart:this.addToCart,
              openModal:this.openModal,
              closeModal:this.closeModal,
              increment:this.increment,
              decrement:this.decrement,
              removeItem:this.removeItem,
              clearCart:this.clearCart,
              filterByType:this.filterByType,
              setProdutc: this.setProdutc
            }}
            >
                {this.props.children}
            </ProductContext.Provider>
            
        )
    }
}

const ProductConsumer = ProductContext.Consumer;

export{ProductProvider,ProductConsumer};