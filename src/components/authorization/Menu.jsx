import React from "react";
import {Link ,withRouter} from "react-router-dom";
import {Signout , isAuthenticated} from "./Auth";
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Button from 'react-bootstrap/Button';
import './Menu.css';




const isActive = (history, path) => {
    if(history.location.pathname === path)
    return {color: "red"}
    // else return {color :"#ffffff"}
}


const Menu =({history}) => (
    
    <div className="autBtn">
            {/* <ul className="nav "> */}
            {/* <Link to ={"/"} className="test1 ">Cheers</Link>  */}
            {/* <ul className="test2 "> */}
            {/* <ul className="nav nav-tabs  justify-content-end" style={{fontFamily:"Arial, Helvetica, sans-serif"}}> */}

        {/* <li className="nav-item"> */}
        {/* <Link className="nav-link" to="/" style={isActive(history, "/")}>Home</Link> */}
        {/* </li> */}
        {/* <li className="nav-item"> */}
        {/* <Link className="nav-link" to="/users" style={isActive(history, "/users")}>Users</Link> */}
        {/* </li> */}
        {!isAuthenticated() && (
        <>
                <li className="nav-item">
        <Link className="nav-link" to="/registration"style={isActive(history, "/registration")}>
        <ButtonToolbar>
            <Button variant="success">Sign Up</Button>
        </ButtonToolbar>
            
            </Link>
        </li>
        <li className="nav-item">
        <Link className="nav-link" to="/signin"style={isActive(history, "/signin")}>
        <ButtonToolbar>
            <Button variant="outline-primary">Sign In</Button>
        </ButtonToolbar></Link>
        </li>
        </>
        )}
        {isAuthenticated() && (
        <>
                <li className="nav-item">

                    
        <Link className="nav-link" to='#'
        style={isActive(history, "/signup")} onClick={()=> Signout(()=> history.push("/"))}>
            <ButtonToolbar>
            <Button variant="danger">Sign Out</Button>
        </ButtonToolbar>
            </Link>
        </li>
        <li className="nav-item">
            
                <Link className="nav-link"
                // style={isActive(history, `/user/${isAuthenticated().user._id}`)}
                to={`/add` }>
                    <ButtonToolbar>
            <Button variant="info">Add Product</Button>
        </ButtonToolbar>

                    {/* {`${isAuthenticated().user.name}'s Profile`} */}
                </Link>

        </li>

        </>
        )}
        {/* </ul> */}
                    {/* </ul> */}
                    
                    
                    {/* </ul> */}
                                    
                    

                

            
    </div>
);

export default withRouter(Menu);