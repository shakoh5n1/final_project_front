import React from 'react';
import './Footer.css'
import {Link} from 'react-router-dom';
// import WrapperComponent from '../shared/wrapper/Wrapper'
// import fbicon from '../../img/fb1x.jpg'

export default class FooterComponent extends React.Component {
    render() {
        return(
            <React.Fragment>
                {/* <WrapperComponent> */}
                    <footer className="footer">
                        <div className="footerContainer">
                            <div className="footerContent">
                                <div className="ftContColum">
                                    <h2 className="footerTitle">Navigation</h2>
                                    <div className="footerTxt">
                                        <span>About us</span>
                                        <span>Vacancy</span>
                                        {/* <span>Address: 314-336 Bourke St, Melbourne VIC 3000, Australia</span> */}
                                    </div>
                                    <div className="social">
                                        {/* <h2 className="footerTitle socialTitle">Follow Us:</h2> */}
                                        {/* <div className="socialIcons"> */}
                                            {/* <img className={fbicon} src="" alt=""/> */}
                                            {/* <img className="ftimg2" src="img/fbx1.JPG" alt=""/> */}
                                            {/* <img className="ftimg3" src="img/ytx1.png" alt=""/> */}
                                        {/* </div> */}
                                    </div>
                                </div>
                                <div className="ftContColum">
                                    <h2 className="footerTitle">Payment</h2>
                                    <div className="footerTxt">
                                        <span>Payment methods</span>
                                        <span>Warranty</span>
                                        <span>Installment</span>
                                        {/* <span>- Booking tips</span> */}
                                    </div>
                                </div>
                                <div className="ftContColum">
                                    <h2 className="footerTitle">Account</h2>
                                    <div className="footerTxt">
                                        <span>Delivery methods</span>                                                                                                                    
                                    </div>
                                </div>
                                <div className="ftContColum">
                                    <Link to="./contact">
                                        <h2 className="footerTitle">Contact</h2>
                                    </Link>
                                    
                                    <div className="footerTxt">
                                        <span>info@zoommer.ge</span>  
                                        <span>+995 (32) 2 60 30 60</span>                                                                                                                    
                                    </div>
                                </div>
                            </div>
                            <div className="footerAddress">
                                
                                    <span>Copyright © 2019</span>
                                    <span>Zoommer.ge</span>
                                    <span>All rights reserved.</span>
                                
                                
                            </div>
                        </div>
                    </footer>
                {/* </WrapperComponent> */}
            </React.Fragment>
        )
    }
}