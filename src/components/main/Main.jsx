import React from 'react';
import './Main.css';
// import {Link} from 'react-router-dom';
// import WrapperComponent from '../shared/wrapper/Wrapper';
import ProductList from '../products/ProductList';


export default class MainComponent extends React.Component {



    render() {
        return(
            <React.Fragment>
                {/* <WrapperComponent> */}
                    {/* <Link to={'/ptoductlist'}>All</Link> */}
                    <ProductList category = "Smart Watches & Fitness Trackers"/>
                    <ProductList category = "Mobile Phones"/>
                    <ProductList category = "Tablets And Accessories"/>
                    <ProductList category = "Laptops & Accessories"/>
                    <ProductList category = "Game Consoles"/>
                    <ProductList category = "Drons"/>
                {/* </WrapperComponent> */}
            </React.Fragment>
        )
    }
}