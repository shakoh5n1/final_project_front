import React from 'react';
import './Slider.css';
import SlideComponent from './Slide';
import { RightArrowComponent, LeftArrowComponent } from './Arrows';


import WrapperComponent from '../shared/wrapper/Wrapper'; 
import NavigationComponent from '../navigation/Navigation';

export default class SliderComponent extends React.Component {
    
    constructor(props) {
        super(props);

        

        this.state = {
            images: [
                "https://img.zoommer.ge/images/thumbs/0103016.jpeg",
                "https://img.zoommer.ge/images/thumbs/0103915.png",
                "https://img.zoommer.ge/images/thumbs/0102306.png",
                "https://img.zoommer.ge/images/thumbs/0102433.jpeg",
                "https://img.zoommer.ge/images/thumbs/0101066.png",
                "https://img.zoommer.ge/images/thumbs/0103016.jpeg",
                "https://img.zoommer.ge/images/thumbs/0103870.jpeg",
                "https://img.zoommer.ge/images/thumbs/0103906.png",
                "https://img.zoommer.ge/images/thumbs/0103909.png",
                "https://img.zoommer.ge/images/thumbs/0103915.png",
                "static/media/0103870.9a30fcae.jpeg",
                
            ],
            currentIndex: 0,
            translateValue: 0,
            hover: false
        }
    }

    goToPrevSlide = () => {
        if(this.state.currentIndex === 0)
        return;
      
      this.setState(prevState => ({
        currentIndex: prevState.currentIndex - 1,
        translateValue: prevState.translateValue + this.slideWidth()
      }))
    }

    goToNextSlide = () => {
        if(this.state.currentIndex === this.state.images.length - 1) {
            return this.setState({
                currentIndex: 0,
                translateValue: 0
            })
        }
        this.setState(prevState => ({
            currentIndex: prevState.currentIndex + 1,
            translateValue: prevState.translateValue + -(this.slideWidth())
        }))
    }

    slideWidth = () => {
        return document.querySelector('.slide').clientWidth
    }

    // startSlider = () => {
    //    setInterval(() => {
    //        this.goToNextSlide()
    //    }, 4000) 
    // }

    // stopSlider = () => {
    //     clearInterval(this.startSlider())
    // }
    componentDidMount() {
        // this.startSlider()
        this.goToNextSlide();
        this.interval = setInterval(this.goToNextSlide,4000)
        
    }
    componentWillUnmount() {
        
        clearInterval(this.interval)
    }

    hoverOn = () => {
        return this.setState({
            hover: true
        })
    }
    hoverOff = () => {
        return this.setState({
            hover: false
        })
    }


    render() {
        return(
            <React.Fragment>
                <div className="slider" onMouseEnter={this.hoverOn} onMouseLeave={this.hoverOff}>
                    <div className="slider-wrapper" style={{transform: `translateX(${this.state.translateValue}px)`,transition: 'transform ease-out 0.45s'}}>
                        {
                            this.state.images.map((image, i) => (
                                <SlideComponent key={i} image={image} />
                        ))
                        }
                    </div>
                    <WrapperComponent>
                        <NavigationComponent />
                        {/* <div className="arrows"> */}
                        <div className={this.state.hover ? "arrows" : "arrowsoff"}>
                            <LeftArrowComponent goToPrevSlide={this.goToPrevSlide} />
                            <RightArrowComponent goToNextSlide={this.goToNextSlide} />
                        </div>
                        
                    </WrapperComponent>
                    
                </div>
            </React.Fragment>
        )
    }
}