import React from 'react';
import './Slider.css';
import '../../img/sliderimages/0103870.jpeg'


const SlideComponent = ({ image }) => {
    const styles = {
        backgroundImage: `url(${image})`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '50% 60%'
      }

    return <div className="slide" style={styles}></div>
}

export default SlideComponent