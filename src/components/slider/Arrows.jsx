import React from 'react';
import './Slider.css';
import Prev from '../../img/leftarrowwhite.png'
import Next from '../../img/rightarrowwhite.png'

const RightArrowComponent = (props) => {
    return (
        <div className="nextArrow arrow">
            <button className="arrBtn arrow nextArrow" onClick={props.goToNextSlide}>
                <img src={Next} alt=""/>
            </button>
        </div>
    )
}

const LeftArrowComponent = (props) => {
    return (
        <div className="backArrow arrow">
            <button className="arrBtn arrow backArrow" onClick={props.goToPrevSlide}>
                <img src={Prev} alt=""/>
            </button>
        </div>
    )
}

export {RightArrowComponent, LeftArrowComponent};