import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
//import AppBar from 'material-ui/AppBar';
import { List, ListItem } from 'material-ui/List';
import RaisedButton from 'material-ui/RaisedButton';

export class Confirm extends Component {
  continue = e => {
    e.preventDefault();
    // PROCESS FORM //
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const {
      values: { firstName, lastName, email, occupation, city, bio }
    } = this.props;
    return (
      <MuiThemeProvider>
        <React.Fragment>
          {/* <AppBar title="Confirm User Data" /> */}
          <List>
            <ListItem important primaryText="Name" secondaryText={firstName} />
            <ListItem primaryText="Surname" secondaryText={lastName} />
            <ListItem primaryText="Mail" secondaryText={email} />
            <ListItem primaryText="Phone" secondaryText={occupation} />
            <ListItem primaryText="Organisation Name" secondaryText={city} />
            <ListItem primaryText="Tel :" secondaryText={bio} />
          </List>
          <br />
          <RaisedButton
            label="Confirm and continue"
            primary={true}
            style={styles.button}
            onClick={this.continue}
          />
          <RaisedButton
            label="Back"
            primary={false}
            style={styles.button}
            onClick={this.back}
          />
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

const styles = {
  button: {
    margin: 40
  }
};

export default Confirm;
