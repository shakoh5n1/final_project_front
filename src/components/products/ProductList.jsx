import React, { Component } from 'react';
import Product from './Product';
import './Products.css';
import Title from './Title';
// import {storeProducts} from '../../data';
import ProductService from '../../Service';
import {ProductConsumer} from '../context/context'



export default class ProductList extends Component {
    constructor(props) {
        super(props);

        this.productService = new ProductService();

        // this.state={
        //     products: this.productService.getProductList()
        // };
    }
    
    render() {
    

        return (
          <React.Fragment>
              

              <div className="py-5">
               <div className="container">
                <Title name=''  title={(this.props.category ? this.props.category : 'All Products')} />
                  <div className="row ">
                  
                   <ProductConsumer>
                  
                       {(value)=>{

                           let tempArray = value.products 
                          

                           if(this.props.category) {
                                tempArray = value.products.filter((product) => {
                                   return product.type === this.props.category
                               })
                               console.log('this type' , value.products)
                           }
                        return tempArray.map(product => {
                            return <Product key={product._id} product={product}
                            />
                        })
                       }}
               
                   </ProductConsumer>
                  </div>
               </div>
              </div>
          </React.Fragment>
        )
    }
}
