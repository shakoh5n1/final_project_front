import React, { Component } from 'react'
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {ProductConsumer} from '../context/context';
import PropTypes from 'prop-types';
// import BaskImg from '../../img/noun_Shopping.svg';
import BaskImgW from '../../img/basket_white.svg';
import addImg from '../../img/in_stock.svg';
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'




export default class Product extends Component {
    render() {
        const {_id,title,img,price,inCart}=this.props.product;

        return (
            <ProductWrapper >
             
                 <Card style={{ width: '18rem' }}>
                    <ProductConsumer>
                    
                        {(value) => ( <div className='padding' style={{ width: '18rem' }} onClick={()=> value.handleDetail(_id)
                        }
                        >
                        <Link to={'/details/'+ _id}>
                            <Card.Img variant="top" src={`http://localhost:5000/${img}`} alt="product"/>
                        </Link>
                        
                        <Card.Body>
                            <Card.Title>{title}</Card.Title>
                            <Card.Text>${price}</Card.Text>
                             <Button variant="primary"
                            disabled={inCart ? true : false}
                            onClick={() => {
                                value.addToCart(_id);
                                value.openModal(_id);
                                }}
                            >
                                {inCart?(
                                <p className="text-capitalize mb-0" >
                                    {" "}
                                    <img src={addImg} alt="bask " className="basket blue" />
                                    </p>
                                    ):(
                                    //   <i className="fas fa-cart-plus"/>
                                    <div>
                                    {/* <img src={BaskImg} alt="bask " className="basket blue" /> */}
                                    <img src={BaskImgW} alt="bask " className="basket white" />
                                    </div>
                                    )}  
                            </Button> 
                        </Card.Body>
                        </div>)}
                    </ProductConsumer>
                    
                </Card>

            
                
            </ProductWrapper>
            
        )
    }
}

Product.propTypes = {
    product:PropTypes.shape({
        id:PropTypes.number,
        img:PropTypes.string,
        title:PropTypes.string,
        price:PropTypes.number,
        inCart:PropTypes.bool
    }).isRequired
}

const ProductWrapper = styled.div`
    .card{
        border-color:transparent;
        // transition: all 1s linear;
    }
    .card-footer{
        background:transparent;
        border-top:transparent;
        // transition:all ;
    }
    &:hover{
        .card{
            // border:0.04rem solid rgba(0,0,0,0.2);
            // box-shadow: 2px 2px 5px 0px rgba (0,0,0,0.2)
            
        }
        .card-footer{
            // background: rgba(247,247,247);
        }
    }
    .img-container{
        position:relative;
        overflow:hidden;
        
    }
    .card-img-top{
    //   transition:all 1s linear;
    }
    .img-container:hover .card-img-top{
        // transform:scale(1.2)
        
    }
    .cart-btn{
        position:absolute;
        bottom:0;
        rigth:0;
        padding:0.2rem 0.4rem;
        background: var(--lightBlue);
        color:var(--mainWhite);
        font-size:1.4rem;
        // border-radius:0.5rem 0 0 0;
        border: none;
        // transform:translate(100% ,100%);
        // transition:all 1s linear;
        width: 50px;
        height: 50px;
        background-color: #f5f5f5f;
    }
    .img-container:hover .cart-btn{
        // transform:translate(0,0)
        color: #ff8b11;
    }
    .cart-btn:hover{
        color:var(--mainBlue);
        cursor: pointer;
        color: #ff8b11;

    }
`
;
