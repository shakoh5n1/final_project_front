import React from 'react';
import HomePage from './pages/homePage/HomePage'
import Details from './pages/productDetailsPage/ProductDetails'
import Cart from './components/header/Cart/Cart';
import AddProduct from './pages/addProduct/AddProduct';
// import UserForm from './pages/addProduct/addPhoto'
import {Switch, Route} from 'react-router-dom';
import ProductList from './pages/productListPage/ProductList'
import 'bootstrap/dist/css/bootstrap.min.css';
import Registration from './components/authorization/Register';
import LoginComponent from './components/authorization/LoginComponent'
// import FormPage from './components/authorization/FormPage';
import Contact from './pages/contactPage/Contact';


export default class AppComponent extends React.Component {
    render() {
        return (
            
            <React.Fragment>
                
                <Switch>
                    <Route exact path="/" component={HomePage}/> 
                    <Route path="/ptoductlist" component={ProductList} />
                    <Route path="/details/:id" component={Details}/>
                    <Route path="/cart" component={Cart}/> 
                    <Route path="/add" component={AddProduct}/> 
                    <Route path = '/registration' component={Registration}/>
                    <Route path = '/signin' component={LoginComponent}/>
                    <Route path ='/contact' component={Contact} />
                </Switch>   
            </React.Fragment>
        ) 
    }
}


